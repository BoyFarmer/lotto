import { Component, OnInit, Renderer } from '@angular/core';

@Component({
    selector: 'app-components',
    templateUrl: './components.component.html',
    styles: [`
      .lottoimg {
        width: 100%;
      }
    `]
})

export class ComponentsComponent implements OnInit {
  user = undefined;  
  welcome = false;
  constructor() {}
    
    ngOnInit() {
      setTimeout(this.get, 1000);
    }

    get () {
      const requestOptions = {
        method: 'get',
        headers: { 'Content-Type': 'application/json'},
        responseType: 'application/json',
      };
      fetch("localhost:5000", requestOptions)
        .then((response) => {
          return response.text();
        }).then(res => {
          if (res === 'None') {
            this.user = undefined
            this.welcome = false;
          } else {
            if(res !+ this.user) {
              this.welcome = true;
            } else {
              this.welcome = false;
            }
            this.user = res;
          }
          setTimeout(this.get, 2000);
        }).catch(r => {
          setTimeout(this.get, 2000);  
        });
    }
}
